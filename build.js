var path = require('path');
var webpack = require('webpack');

var config = {
	entry: './magicsuggest.js',
	output: {
        filename: 'magicsuggest.js',
		path: path.join(__dirname, 'dist')
	}
};

var compiler = webpack(config);
compiler.run(function(){});
